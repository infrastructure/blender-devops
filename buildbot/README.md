# Blender Buildbot

## Dependencies

This repository should be located at `~/git/blender-devops`.

```sh
sudo apt-get install -y p7zip-full dmg2img
pip3 install -r requirements.txt
```

## Workers

Each executable script in `worker/` corresponds to a builder on the buildbot.
Run with the `--help` option to see the available build steps and arguments.

For example:
```sh
# Full pipeline
./buildbot/worker/doc_developer.py all
# Individual step
./buildbot/worker/doc_developer.py compile
```

This is the most convenient way to test and debug, without going through the
buildbot interface.

## Pipelines

The configuration of tracks and builders on the servers is in `pipeline/`.
There is a corresponding pipeline script for every worker script.

## Configuration

### Local

By default everything runs in the LOCAL service environment, using the
configuration from `conf/local`. It performs dry runs when not on the
right platform or credentials are missing, to make local testing easier.

Packages and docs are delivered to `~/git/delivery` for testing.

### Production

The configuration for UATEST and PROD service environment is expected
to be in `conf/production`. These files are not included in the repository
as they are highly tied to the Blender server infrastructure.

## Buildbot

### Setup

Create master and worker:
```
mkdir -p ~/.devops/services/buildbot-master
cd  ~/.devops/services/buildbot-master
pipenv install --python 3.9 -r ~/git/bdr-devops-core/buildbot/requirements.txt
pipenv run buildbot create-master -r .
cp ~/git/bdr-devops-core/buildbot/master.cfg .
```

```
mkdir -p ~/.devops/services/buildbot-worker
cd ~/.devops/services/buildbot-worker
pipenv install buildbot-worker PyYAML psutil
pipenv run buildbot-worker create-worker --umask=0o22 -r . localhost localhost localhost
```

### Run

Start:
```
cd ~/.devops/services/buildbot-master && pipenv run buildbot start
cd ~/.devops/services/buildbot-worker && pipenv run buildbot-worker start
```

For local testing, the admin interface will be available at
<http://localhost:8010>, with username `admin` and password `admin`.

Update:
```
buildbot reconfig ~/.devops/services/buildbot-master
```

Stop:
```
buildbot stop ~/.devops/services/buildbot-master
buildbot-worker stop ~/.devops/services/buildbot-worker
```
