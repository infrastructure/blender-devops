# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os
import pathlib

from typing import Optional, Tuple

# Where tracks data is stored.
tracks_root_path = pathlib.Path.home() / "git"

# Software cache
software_cache_path = tracks_root_path / "downloads" / "software" / "workers"

# Docs delivery.
# Manual
docs_manual_user = os.getlogin()
docs_manual_machine = "127.0.0.1"
docs_manual_folder = tracks_root_path / "delivery" / "docs"
docs_manual_port = 22

# API
docs_api_user = os.getlogin()
docs_api_machine = "127.0.0.1"
docs_api_folder = tracks_root_path / "delivery" / "docs"
docs_api_port = 22

# Developer docs
docs_dev_user = os.getlogin()
docs_dev_machine = "127.0.0.1"
docs_dev_folder = tracks_root_path / "delivery" / "docs"
docs_dev_port = 22

# Studio docs delivery.
studio_user = os.getlogin()
studio_machine = "127.0.0.1"
studio_folder = tracks_root_path / "delivery" / "studio" / "blender-studio-tools"
studio_port = 22

# Download delivery.
download_user = os.getlogin()
download_machine = "127.0.0.1"
download_source_folder = tracks_root_path / "delivery" / "download" / "source"
download_release_folder = tracks_root_path / "delivery" / "download" / "release"
download_port = 22

# Buildbot download delivery
buildbot_download_folder = tracks_root_path / "delivery" / "buildbot"

# Code signing
sign_code_windows_sign_tool_path = (
    "C:\\Tools\\Microsoft.Windows.SDK.BuildTools\\bin\\10.0.22621.0\\x64\\signtool.exe"
)
sign_code_windows_timestamp_url = "http://timestamp.acs.microsoft.com"
sign_code_windows_dlib_path = (
    "C:\\Tools\\Microsoft.Trusted.Signing.Client\\bin\\x64\\Azure.CodeSigning.Dlib.dll"
)
sign_code_windows_metadata_path = "C:\\Tools\\metadata.json"
sign_code_windows_publisher_id = "fake_windows_store_publisher"
sign_code_darwin_certificate = None
sign_code_darwin_team_id = None
sign_code_darwin_apple_id = None
sign_code_darwin_keychain_profile = None


def darwin_keychain_password(service_env_id: str) -> str:
    return "fake_keychain_password"


# Steam
steam_app_id = None
steam_platform_depot_ids = {
    "windows": None,
    "linux": None,
    "darwin": None,
}


def steam_credentials(service_env_id: str) -> Tuple[str, str]:
    return "fake_steam_username", "fake_steam_password"


# Snap
def snap_credentials(service_env_id: str) -> str:
    return "fake_snap_credentials"


# PyPI
def pypi_token(service_env_id: str) -> str:
    return "fake_pypi_token"


# Gitea
def gitea_api_token(service_env_id: str) -> Optional[str]:
    return None
