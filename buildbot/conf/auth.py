# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import importlib

import buildbot.plugins


def _get_auth_config(devops_env_id: str):
    if devops_env_id == "LOCAL":
        import conf.local.auth

        importlib.reload(conf.local.auth)
        return conf.local.auth
    else:
        import conf.production.auth

        importlib.reload(conf.production.auth)
        return conf.production.auth


def fetch_authentication(devops_env_id: str):
    auth_config = _get_auth_config(devops_env_id)
    return auth_config.get_authentication(devops_env_id)


def fetch_authorization(devops_env_id: str):
    auth_config = _get_auth_config(devops_env_id)

    admin_usernames = auth_config.admin_usernames
    deploy_dev_usernames = auth_config.deploy_dev_usernames
    trusted_dev_usernames = auth_config.trusted_dev_usernames

    dev_usernames = list(set(deploy_dev_usernames + trusted_dev_usernames + admin_usernames))
    deploy_usernames = list(set(deploy_dev_usernames + admin_usernames))

    file_based_group_username_role_matchers = [
        buildbot.plugins.util.RolesFromUsername(roles=["admin"], usernames=admin_usernames),
        buildbot.plugins.util.RolesFromUsername(roles=["deploy"], usernames=deploy_usernames),
        buildbot.plugins.util.RolesFromUsername(roles=["dev"], usernames=dev_usernames),
    ]

    my_authz = buildbot.plugins.util.Authz(
        stringsMatcher=buildbot.plugins.util.fnmatchStrMatcher,
        allowRules=[
            # Admins can do anything,
            #
            # defaultDeny=False: if user does not have the admin role, we continue
            # parsing rules
            # buildbot.plugins.util.AnyEndpointMatcher(role='admin', defaultDeny=False),
            # buildbot.plugins.util.AnyEndpointMatcher(role='dev', defaultDeny=False),
            # buildbot.plugins.util.AnyEndpointMatcher(role='coordinator', defaultDeny=False),
            # buildbot.plugins.util.AnyEndpointMatcher(role='anonymous', defaultDeny=False),
            buildbot.plugins.util.StopBuildEndpointMatcher(role="dev", defaultDeny=True),
            buildbot.plugins.util.RebuildBuildEndpointMatcher(role="dev", defaultDeny=True),
            buildbot.plugins.util.EnableSchedulerEndpointMatcher(role="admin", defaultDeny=True),
            # buildbot.plugins.util.AnyEndpointMatcher(role='any', defaultDeny=False),
            # Force roles
            buildbot.plugins.util.ForceBuildEndpointMatcher(
                builder="*-code-experimental-*", role="dev", defaultDeny=True
            ),
            buildbot.plugins.util.ForceBuildEndpointMatcher(
                builder="*-code-patch-*", role="dev", defaultDeny=True
            ),
            buildbot.plugins.util.ForceBuildEndpointMatcher(
                builder="*-code-daily-*", role="dev", defaultDeny=True
            ),
            buildbot.plugins.util.ForceBuildEndpointMatcher(
                builder="*-store-*", role="deploy", defaultDeny=True
            ),
            buildbot.plugins.util.ForceBuildEndpointMatcher(
                builder="*-deploy-*", role="deploy", defaultDeny=True
            ),
            buildbot.plugins.util.ForceBuildEndpointMatcher(
                builder="*-doc-*", role="dev", defaultDeny=True
            ),
            # Rebuild roles
            buildbot.plugins.util.RebuildBuildEndpointMatcher(
                builder="*-code-experimental-*", role="dev", defaultDeny=True
            ),
            buildbot.plugins.util.RebuildBuildEndpointMatcher(
                builder="*-code-patch-*", role="dev", defaultDeny=True
            ),
            buildbot.plugins.util.RebuildBuildEndpointMatcher(
                builder="*-code-daily-*", role="dev", defaultDeny=True
            ),
            buildbot.plugins.util.RebuildBuildEndpointMatcher(
                builder="*-store-*", role="deploy", defaultDeny=True
            ),
            buildbot.plugins.util.RebuildBuildEndpointMatcher(
                builder="*-deploy-*", role="deploy", defaultDeny=True
            ),
            buildbot.plugins.util.RebuildBuildEndpointMatcher(
                builder="*-doc-*", role="dev", defaultDeny=True
            ),
            # This also affects starting jobs via force scheduler
            buildbot.plugins.util.AnyControlEndpointMatcher(role="admin", defaultDeny=True),
            # A default deny for any endpoint if not admin
            # If this is missing at the end, any UNMATCHED group will get 'allow'...
            buildbot.plugins.util.AnyControlEndpointMatcher(role="admin", defaultDeny=True),
        ],
        roleMatchers=file_based_group_username_role_matchers,
    )

    return my_authz
