# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2018 LAB132
# SPDX-FileCopyrightText: 2013-2024 Blender Authors
# <pep8 compliant>

# Based on the gitlab reporter from buildbot

from twisted.python import log

import buildbot.plugins

import importlib
import requests

import gitea.reporter

importlib.reload(gitea.reporter)

# Create status reporter service.
gitea_url = "https://projects.blender.org"
gitea_api_token = None
gitea_status_service = None


def setup_service(devops_env_id: str):
    import conf.worker

    importlib.reload(conf.worker)
    worker_config = conf.worker.get_config(devops_env_id)
    gitea_api_token = worker_config.gitea_api_token(devops_env_id)

    if gitea_api_token:
        log.msg("Found Gitea API token, enabling status push")
        return gitea.reporter.GiteaStatusService11(gitea_url, gitea_api_token, verbose=False)
    else:
        log.msg("No Gitea API token found, status push disabled")
        return None


# Get revision for coordinator.
@buildbot.plugins.util.renderer
def get_patch_revision(props):
    if "revision" in props and props["revision"]:
        return {}
    if "pull_revision" in props and props["pull_revision"]:
        return {"revision": props["pull_revision"]}
    pull_id = props["patch_id"]
    url = f"{gitea_url}/api/v1/repos/blender/blender/pulls/{pull_id}"
    response = requests.get(url, headers={"accept": "application/json"})
    sha = response.json().get("head", {"sha": ""}).get("sha")
    return {"revision": sha}


@buildbot.plugins.util.renderer
def get_branch_revision(props):
    if "revision" in props and props["revision"]:
        return {}
    branch = props["override_branch_id"]
    url = f"{gitea_url}/api/v1/repos/blender/blender/git/commits/{branch}"
    response = requests.get(url, headers={"accept": "application/json"})
    sha = response.json().get("sha", "")
    return {"revision": sha}
