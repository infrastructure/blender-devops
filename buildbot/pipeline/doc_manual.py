# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import buildbot.plugins

import conf.branches
import pipeline.common


def populate(devops_env_id):
    properties = [
        buildbot.plugins.util.BooleanParameter(
            name="needs_package_delivery",
            label="Package delivery -> push build to configured services",
            required=True,
            strict=True,
            default=True,
        ),
        buildbot.plugins.util.BooleanParameter(
            name="needs_all_locales",
            label="All locales -> process all configure locales",
            required=True,
            strict=True,
            default=False,
        ),
    ]

    return pipeline.common.create_pipeline(
        devops_env_id,
        "doc-manual",
        "doc_manual.py",
        ["configure-machine", "update", "compile", "package", "deliver", "clean"],
        conf.branches.code_tracked_branch_ids,
        properties,
        "blender-manual.git",
        ["linux-x86_64-general", "linux-x86_64-general"],
        variation_property="doc_format",
        variations=["html", "epub"],
        incremental_properties={"needs_package_delivery": True, "needs_all_locales": False},
        nightly_properties={"needs_package_delivery": True, "needs_all_locales": True},
        tree_stable_timer_in_seconds=15 * 60,
        do_step_if=pipeline.common.needs_do_doc_pipeline_step,
    )
