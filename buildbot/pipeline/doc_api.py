# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import buildbot.plugins

import conf.branches
import pipeline.common


def populate(devops_env_id):
    properties = [
        buildbot.plugins.util.BooleanParameter(
            name="needs_full_clean",
            label="Full clean -> removes build workspace on machine",
            required=True,
            strict=True,
            default=False,
        ),
        buildbot.plugins.util.BooleanParameter(
            name="needs_package_delivery",
            label="Package delivery -> push build to configured services",
            required=True,
            strict=True,
            default=False,
        ),
    ]

    return pipeline.common.create_pipeline(
        devops_env_id,
        "doc-api",
        "doc_api.py",
        [
            "configure-machine",
            "update-code",
            "compile-code",
            "compile-install",
            "compile",
            "package",
            "deliver",
            "clean",
        ],
        conf.branches.code_tracked_branch_ids,
        properties,
        "blender.git",
        ["linux-x86_64-general"],
        variations=["html"],
        incremental_properties={"needs_package_delivery": False},
        nightly_properties={"needs_package_delivery": True},
        tree_stable_timer_in_seconds=15 * 60,
        do_step_if=pipeline.common.needs_do_doc_pipeline_step,
        hour=1,
        minute=30,
    )
