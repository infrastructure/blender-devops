# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

# Builders for deploying Python module releases to PyPI.

import conf.branches
import pipeline.common


def populate(devops_env_id):
    properties = []

    return pipeline.common.create_pipeline(
        devops_env_id,
        "code-bpy-deploy",
        "code_bpy_deploy.py",
        [
            "configure-machine",
            "update-code",
            "pull",
            "deliver-pypi",
            "clean",
        ],
        conf.branches.code_deploy_track_ids,
        properties,
        "blender.git",
        ["linux-x86_64-general"],
    )
