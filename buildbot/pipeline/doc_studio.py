# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import buildbot.plugins

import pipeline.common


def populate(devops_env_id):
    properties = [
        buildbot.plugins.util.BooleanParameter(
            name="needs_package_delivery",
            label="Package delivery -> push build to configured services",
            required=True,
            strict=True,
            default=True,
        ),
    ]

    return pipeline.common.create_pipeline(
        devops_env_id,
        "doc-studio-tools",
        "doc_studio.py",
        ["update", "compile", "deliver"],
        {"vdev": "main"},
        properties,
        "blender-studio-tools.git",
        ["linux-x86_64-doc-studio-tools"],
        incremental_properties={"needs_package_delivery": True},
        do_step_if=pipeline.common.needs_do_doc_pipeline_step,
    )
