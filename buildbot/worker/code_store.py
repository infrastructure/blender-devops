#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib
import sys

from collections import OrderedDict

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent))

import worker.configure
import worker.utils

import worker.blender
import worker.blender.update

import worker.deploy
import worker.deploy.artifacts
import worker.deploy.snap
import worker.deploy.steam
import worker.deploy.windows


def package(builder: worker.deploy.CodeStoreBuilder) -> None:
    if builder.store_id == "snap":
        worker.deploy.snap.package(builder)
    elif builder.store_id == "steam":
        worker.deploy.steam.package(builder)
    elif builder.store_id == "windows":
        builder.setup_build_environment()
        worker.deploy.windows.package(builder)


def deliver(builder: worker.deploy.CodeStoreBuilder) -> None:
    if builder.store_id == "snap":
        worker.deploy.snap.deliver(builder)
    elif builder.store_id == "steam":
        worker.deploy.steam.deliver(builder)
    elif builder.store_id == "windows":
        worker.deploy.windows.deliver(builder)


if __name__ == "__main__":
    steps: worker.utils.BuilderSteps = OrderedDict()
    steps["configure-machine"] = worker.configure.configure_machine
    steps["update-code"] = worker.blender.update.update
    steps["pull-artifacts"] = worker.deploy.artifacts.pull
    steps["package"] = package
    steps["deliver"] = deliver
    steps["clean"] = worker.deploy.CodeDeployBuilder.clean

    parser = worker.blender.create_argument_parser(steps=steps)
    parser.add_argument("--store-id", type=str, choices=["snap", "steam", "windows"], required=True)

    args = parser.parse_args()
    builder = worker.deploy.CodeStoreBuilder(args)
    builder.run(args.step, steps)
