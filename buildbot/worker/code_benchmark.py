#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import argparse
import pathlib
import sys

from collections import OrderedDict

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent))

import worker.configure
import worker.utils

import worker.blender
import worker.blender.benchmark
import worker.blender.compile
import worker.blender.update


class BenchmarkBuilder(worker.blender.CodeBuilder):
    def __init__(self, args: argparse.Namespace):
        super().__init__(args)
        self.setup_track_path()


if __name__ == "__main__":
    steps: worker.utils.BuilderSteps = OrderedDict()
    steps["configure-machine"] = worker.configure.configure_machine
    steps["update-code"] = worker.blender.update.update
    steps["compile-code"] = worker.blender.compile.compile_code
    steps["compile-gpu"] = worker.blender.compile.compile_gpu
    steps["compile-install"] = worker.blender.compile.compile_install
    steps["benchmark"] = worker.blender.benchmark.benchmark
    steps["clean"] = worker.blender.CodeBuilder.clean

    parser = worker.blender.create_argument_parser(steps=steps)

    args = parser.parse_args()
    builder = BenchmarkBuilder(args)
    builder.run(args.step, steps)
