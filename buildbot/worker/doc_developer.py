#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import argparse
import os
import pathlib
import sys

from collections import OrderedDict

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent))

import worker.utils


class DocDeveloperBuilder(worker.utils.Builder):
    def __init__(self, args: argparse.Namespace):
        super().__init__(args, "blender", "blender-developer-docs")
        self.build_path = self.track_path / "build_developer_docs"
        self.output_path = self.build_path / "html"
        self.setup_track_path()


def update(builder: DocDeveloperBuilder) -> None:
    builder.update_source()


def compile_doc(builder: DocDeveloperBuilder) -> None:
    os.chdir(builder.track_path)
    worker.utils.call_pipenv(["install", "--requirements", builder.code_path / "requirements.txt"])

    worker.utils.remove_dir(builder.output_path)

    os.makedirs(builder.build_path, exist_ok=True)
    os.chdir(builder.build_path)

    mkdocs_yml_path = builder.code_path / "mkdocs.yml"
    worker.utils.call_pipenv(
        ["run", "mkdocs", "build", "-f", mkdocs_yml_path, "-d", builder.output_path]
    )


def deliver(builder: DocDeveloperBuilder) -> None:
    worker_config = builder.get_worker_config()

    remote_path = f"developer.blender.org/webroot/{builder.service_env_id}/docs"

    connect_id = f"{worker_config.docs_dev_user}@{worker_config.docs_dev_machine}"
    server_docs_path = pathlib.Path(worker_config.docs_dev_folder) / pathlib.Path(remote_path)

    change_modes = ["D0755", "F0644"]
    source_path = f"{builder.output_path}/"
    dest_path = f"{connect_id}:{server_docs_path}/"

    worker.utils.call_ssh(
        connect_id=connect_id,
        cmd=["mkdir", "-p", server_docs_path],
        port=worker_config.docs_dev_port,
    )
    worker.utils.rsync(
        source_path,
        dest_path,
        change_modes=change_modes,
        port=worker_config.docs_dev_port,
        delete=True,
        delete_path_check=f"/developer.blender.org/webroot/{builder.service_env_id}/docs",
    )


if __name__ == "__main__":
    steps: worker.utils.BuilderSteps = OrderedDict()
    steps["update"] = update
    steps["compile"] = compile_doc
    steps["deliver"] = deliver

    parser = worker.utils.create_argument_parser(steps=steps)
    parser.add_argument("--needs-package-delivery", action="store_true", required=False)

    args = parser.parse_args()
    builder = DocDeveloperBuilder(args)
    builder.run(args.step, steps)
