# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib
import re

import worker.blender


class VersionInfo:
    def __init__(self, builder: worker.blender.CodeBuilder):
        # Get version information
        buildinfo_h = builder.build_dir / "source" / "creator" / "buildinfo.h"
        blender_h = (
            builder.blender_dir / "source" / "blender" / "blenkernel" / "BKE_blender_version.h"
        )

        version_number = int(self._parse_header_file(blender_h, "BLENDER_VERSION"))

        version_number_patch = int(self._parse_header_file(blender_h, "BLENDER_VERSION_PATCH"))
        self.major, self.minor, self.patch = (
            version_number // 100,
            version_number % 100,
            version_number_patch,
        )

        if self.major >= 3:
            self.short_version = "%d.%d" % (self.major, self.minor)
            self.version = "%d.%d.%d" % (self.major, self.minor, self.patch)
        else:
            self.short_version = "%d.%02d" % (self.major, self.minor)
            self.version = "%d.%02d.%d" % (self.major, self.minor, self.patch)

        self.version_cycle = self._parse_header_file(blender_h, "BLENDER_VERSION_CYCLE")
        if buildinfo_h.exists():
            self.hash = self._parse_header_file(buildinfo_h, "BUILD_HASH")[1:-1]
        else:
            self.hash = ""
        self.risk_id = self.version_cycle.replace("release", "stable").replace("rc", "candidate")
        self.is_development_build = self.version_cycle == "alpha"

    def _parse_header_file(self, filename: pathlib.Path, define: str) -> str:
        regex = re.compile(r"^#\s*define\s+%s\s+(.*)" % define)
        with open(filename, "r") as file:
            for line in file:
                match = regex.match(line)
                if match:
                    return match.group(1)

        raise BaseException(f"Failed to parse {filename.name} header for {define}")
