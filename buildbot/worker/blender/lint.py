# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os
import sys

import worker.blender
import worker.utils


def make_format(builder: worker.blender.CodeBuilder) -> bool:
    os.chdir(builder.blender_dir)

    # Always run formatting with scripts from main, for security on unverified patches.
    # TODO: how secure is this? How to test formatting issues in the scripts themselves?
    # main_files = [makefile, "tools/utils_maintenance", "build_files/windows"]
    # for main_file in main_files:
    #     worker.utils.call(['git', 'checkout', 'origin/main', '--', main_file])

    # Run format
    if builder.platform == "windows":
        builder.call(["make.bat", "format"])
    else:
        builder.call(["make", "-f", "GNUmakefile", "format"])

    # Check for changes
    diff = worker.utils.check_output(["git", "diff"])
    if len(diff) > 0:
        print(diff)

    # Reset
    worker.utils.call(["git", "checkout", "HEAD", "--", "."])

    if len(diff) > 0:
        worker.utils.error('Incorrect formatting detected, run "make format" to fix')
        return False

    return True


def lint(builder: worker.blender.CodeBuilder) -> None:
    ok = make_format(builder)
    if not ok:
        sys.exit(1)
