# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib

import subprocess
from typing import Optional, Sequence

import worker.blender
import worker.utils


def is_signed(service_env_id: str, file_path: pathlib.Path) -> bool:
    import conf.worker

    worker_config = conf.worker.get_config(service_env_id)
    sign_tool_path = worker_config.sign_code_windows_sign_tool_path

    result = subprocess.run(
        [sign_tool_path, "verify", "/pa", str(file_path)], capture_output=True, text=True
    )
    return result.returncode == 0


def sign_windows_files(
    service_env_id: str,
    file_paths: Sequence[pathlib.Path],
    description: Optional[str] = None,
) -> None:
    import conf.worker

    worker_config = conf.worker.get_config(service_env_id)
    sign_tool_path = worker_config.sign_code_windows_sign_tool_path
    timestamp_url = worker_config.sign_code_windows_timestamp_url
    dlib_path = worker_config.sign_code_windows_dlib_path
    metadata_path = worker_config.sign_code_windows_metadata_path

    dry_run = False
    if service_env_id == "LOCAL":
        worker.utils.warning("Performing dry run on LOCAL service environment")
        dry_run = True

    cmd_args = [
        sign_tool_path,
        "sign",
        "/v",
        "/debug",
        "/fd",
        "SHA256",
        "/tr",
        timestamp_url,
        "/td",
        "SHA256",
        "/dlib",
        dlib_path,
        "/dmdf",
        metadata_path,
    ]
    if description:
        cmd_args += ["/d", description]

    cmd: worker.utils.CmdSequence = cmd_args + list(file_paths)
    worker.utils.call(cmd, retry_count=2, dry_run=dry_run)


def sign_windows(service_env_id: str, install_path: pathlib.Path) -> None:
    worker.utils.info("Collecting files to process")

    # find all .exe files that aren't from our libs and
    # haven't been signed yet
    file_paths = [
        f
        for f in install_path.rglob("*.exe")
        if "lib" not in f.parts and not is_signed(service_env_id, f)
    ]

    if file_paths:
        sign_windows_files(service_env_id, file_paths)
        worker.utils.info("Signed {} files.".format(len(file_paths)))
    else:
        worker.utils.info("No unsigned files found.")

    worker.utils.info("End of codesign steps")


def sign_darwin_files(
    builder: worker.blender.CodeBuilder,
    file_paths: Sequence[pathlib.Path],
    entitlements_file_name: str,
) -> None:
    entitlements_path = builder.code_path / "release" / "darwin" / entitlements_file_name

    if not entitlements_path.exists():
        raise Exception(f"File {entitlements_path} not found, aborting")

    worker_config = builder.get_worker_config()
    certificate_id = worker_config.sign_code_darwin_certificate

    dry_run = False
    if builder.service_env_id == "LOCAL" and not certificate_id:
        worker.utils.warning("Performing dry run on LOCAL service environment")
        dry_run = True

    keychain_password = worker_config.darwin_keychain_password(builder.service_env_id)
    cmd: worker.utils.CmdSequence = [
        "security",
        "unlock-keychain",
        "-p",
        worker.utils.HiddenArgument(keychain_password),
    ]
    worker.utils.call(cmd, dry_run=dry_run)

    for file_path in file_paths:
        if file_path.is_dir() and file_path.suffix != ".app":
            continue

        # Remove signature
        if file_path.suffix != ".dmg":
            worker.utils.call(
                ["codesign", "--remove-signature", file_path], exit_on_error=False, dry_run=dry_run
            )

        # Add signature
        worker.utils.call(
            [
                "codesign",
                "--force",
                "--timestamp",
                "--options",
                "runtime",
                f"--entitlements={entitlements_path}",
                "--sign",
                certificate_id,
                file_path,
            ],
            retry_count=3,
            dry_run=dry_run,
        )
        if file_path.suffix == ".app":
            worker.utils.info(f"Vaildating app bundle {file_path}")
            worker.utils.call(
                ["codesign", "-vvv", "--deep", "--strict", file_path], dry_run=dry_run
            )


def sign_darwin(builder: worker.blender.CodeBuilder) -> None:
    bundle_path = builder.install_dir / "Blender.app"

    # Executables
    sign_path = bundle_path / "Contents" / "MacOS"
    worker.utils.info(f"Collecting files to process in {sign_path}")
    sign_darwin_files(builder, list(sign_path.rglob("*")), "entitlements.plist")

    # Thumbnailer app extension.
    thumbnailer_appex_path = bundle_path / "Contents" / "PlugIns" / "blender-thumbnailer.appex"
    if thumbnailer_appex_path.exists():
        sign_path = thumbnailer_appex_path / "Contents" / "MacOS"
        worker.utils.info(f"Collecting files to process in {sign_path}")
        sign_darwin_files(builder, list(sign_path.rglob("*")), "thumbnailer_entitlements.plist")

    # Shared librarys and Python
    sign_path = bundle_path / "Contents" / "Resources"
    worker.utils.info(f"Collecting files to process in {sign_path}")
    file_paths = list(
        set(sign_path.rglob("*.dylib"))
        | set(sign_path.rglob("*.so"))
        | set(sign_path.rglob("python3.*"))
    )
    sign_darwin_files(builder, file_paths, "entitlements.plist")

    # Bundle
    worker.utils.info(f"Signing app bundle {bundle_path}")
    sign_darwin_files(builder, [bundle_path], "entitlements.plist")


def sign(builder: worker.blender.CodeBuilder) -> None:
    builder.setup_build_environment()

    if builder.platform == "windows":
        sign_windows(builder.service_env_id, builder.install_dir)
    elif builder.platform == "darwin":
        sign_darwin(builder)
    else:
        worker.utils.info("No code signing to be done on this platform")
