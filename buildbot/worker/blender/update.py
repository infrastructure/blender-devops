# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os
import sys

import worker.blender
import worker.utils


def _clean_folders(builder: worker.blender.CodeBuilder) -> None:
    # Delete build folders.
    if builder.needs_full_clean:
        worker.utils.remove_dir(builder.build_dir)
    else:
        worker.utils.remove_dir(builder.build_dir / "Testing")
        worker.utils.remove_dir(builder.build_dir / "bin" / "tests")

    # Delete install and packaging folders
    worker.utils.remove_dir(builder.install_dir)
    worker.utils.remove_dir(builder.package_dir)


def update(builder: worker.blender.CodeBuilder) -> None:
    _clean_folders(builder)

    builder.update_source()
    os.chdir(builder.code_path)

    make_update_path = builder.code_path / "build_files" / "utils" / "make_update.py"

    make_update_text = make_update_path.read_text()
    if "def svn_update" in make_update_text:
        worker.utils.error("Can't build branch or pull request that uses Subversion libraries.")
        worker.utils.error("Merge with latest main or release branch to use Git LFS libraries.")
        sys.exit(1)

    # Run make update
    cmd = [
        sys.executable,
        make_update_path,
        "--no-blender",
        "--use-linux-libraries",
        "--use-tests",
        "--architecture",
        builder.architecture,
    ]

    if builder.track_id not in ("v360", "vexp"):
        cmd += ["--prune-destructive"]

    worker.utils.call(cmd)
