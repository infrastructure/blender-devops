# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os

import worker.blender.version
import worker.deploy
import worker.utils


def _package(builder: worker.deploy.CodeDeployBuilder, needs_complete: bool = False) -> None:
    os.chdir(builder.code_path)
    if needs_complete:
        worker.utils.call(["make", "source_archive_complete"])
    else:
        worker.utils.call(["make", "source_archive"])

    # The make change scripts writes to a different location since 2.83.
    for source_file in builder.code_path.glob("blender-*.tar.xz*"):
        worker.utils.move(source_file, builder.package_source_dir / source_file.name)
    for source_file in builder.track_path.glob("blender-*.tar.xz*"):
        worker.utils.move(source_file, builder.package_source_dir / source_file.name)


def package(builder: worker.deploy.CodeDeployBuilder) -> None:
    print(f"Cleaning path [{builder.package_source_dir}]")
    worker.utils.remove_dir(builder.package_source_dir)
    os.makedirs(builder.package_source_dir, exist_ok=True)

    _package(builder, needs_complete=False)

    version_info = worker.blender.version.VersionInfo(builder)
    if version_info.patch != 0:
        worker.utils.info("Skipping complete source package for patch release")
        return

    _package(builder, needs_complete=True)
