# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import json
import os
import urllib.request
import zipfile

import worker.blender
import worker.blender.version
import worker.deploy
import worker.utils


def pull(builder: worker.deploy.CodeDeployBuilder) -> None:
    version_info = worker.blender.version.VersionInfo(builder)

    worker.utils.info("Cleaning package and download directory")
    worker.utils.remove_dir(builder.package_dir)
    worker.utils.remove_dir(builder.download_dir)
    os.makedirs(builder.package_dir, exist_ok=True)
    os.makedirs(builder.download_dir, exist_ok=True)

    # Fetch builds information.
    env_base_url = {
        "LOCAL": "https://builder.blender.org",
        "UATEST": "https://builder.uatest.blender.org",
        "PROD": "https://builder.blender.org",
    }
    base_url = env_base_url[builder.service_env_id]

    search_url = f"{base_url}/download/bpy/?format=json&v=1"

    worker.utils.info(f"Fetching build JSON from [{search_url}]")

    builds_response = urllib.request.urlopen(search_url)
    builds_json = json.load(builds_response)

    # Get builds matching our version.
    worker.utils.info("Processing build JSON")

    matching_builds = []
    for build in builds_json:
        if build["version"] != version_info.version:
            continue
        if not build["file_name"].endswith(".zip"):
            continue
        worker.utils.info(f"Found {build['file_name']}")
        if build["risk_id"] != "stable":
            raise Exception("Can not only deploy stable releases")
        matching_builds.append(build)

    # Check expected platforms
    branches_config = builder.get_branches_config()
    expected_platforms = branches_config.code_official_platform_architectures[builder.track_id]
    if len(expected_platforms) != len(matching_builds):
        platform_names = "\n".join(expected_platforms)
        raise Exception("Unexpected number of builds, expected:\n" + platform_names)

    # Download builds.
    for build in matching_builds:
        file_uri = build["url"]
        file_name = build["file_name"]

        worker.utils.info(f"Download [{file_uri}]")
        download_file_path = builder.download_dir / file_name
        urllib.request.urlretrieve(file_uri, download_file_path)

        # Unzip.
        with zipfile.ZipFile(download_file_path, "r") as zipf:
            zipf.extractall(path=builder.package_dir)

    worker.utils.remove_dir(builder.download_dir)


def deliver(builder: worker.deploy.CodeDeployBuilder) -> None:
    dry_run = builder.service_env_id != "PROD"
    wheels = list(builder.package_dir.glob("*.whl"))

    # Check expected platforms
    branches_config = builder.get_branches_config()
    expected_platforms = branches_config.code_official_platform_architectures[builder.track_id]
    wheel_names = "\n".join([wheel.name for wheel in wheels])
    wheel_paths = [str(wheel) for wheel in wheels]
    print(wheel_names)
    if len(expected_platforms) != len(wheels):
        raise Exception("Unexpected number of wheels:\n" + wheel_names)

    # Check wheels
    cmd = ["twine", "check"] + wheel_paths
    worker.utils.call(cmd)

    # Upload
    worker_config = builder.get_worker_config()
    env = os.environ.copy()
    env["TWINE_USERNAME"] = "__token__"
    env["TWINE_PASSWORD"] = worker_config.pypi_token(builder.service_env_id)
    env["TWINE_REPOSITORY_URL"] = "https://upload.pypi.org/legacy/"

    cmd = ["twine", "upload", "--verbose", "--non-interactive"] + wheel_paths
    worker.utils.call(cmd, env=env, dry_run=dry_run)
