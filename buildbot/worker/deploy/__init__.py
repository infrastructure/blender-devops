# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import argparse
import pathlib

import worker.blender
import worker.utils


class CodeDeployBuilder(worker.blender.CodeBuilder):
    def __init__(self, args: argparse.Namespace):
        super().__init__(args)
        self.platform_ids = ["linux", "darwin", "windows"]
        self.setup_track_path()

        track_path: pathlib.Path = self.track_path

        self.download_dir = track_path / "build_download"
        self.package_source_dir = track_path / "build_source"
        self.store_steam_dir = track_path / "build_store_steam"
        self.store_snap_dir = track_path / "build_store_snap"
        self.store_windows_dir = track_path / "build_store_windows"

    def clean(self):
        worker.utils.remove_dir(self.download_dir)
        worker.utils.remove_dir(self.package_dir)
        worker.utils.remove_dir(self.package_source_dir)
        worker.utils.remove_dir(self.store_steam_dir)
        worker.utils.remove_dir(self.store_snap_dir)
        worker.utils.remove_dir(self.store_windows_dir)
        # Created by make source_archive_complete
        worker.utils.remove_dir(self.track_path / "build_linux")
        worker.utils.remove_dir(self.track_path / "build_darwin")


class CodeStoreBuilder(CodeDeployBuilder):
    def __init__(self, args: argparse.Namespace):
        super().__init__(args)
        self.store_id = args.store_id
